const express = require("express");
const responseTime = require("response-time");
const axios = require("axios");
const redis = require("redis");
const app = express();

//example endpoint: api/search?pokeone=Tynamo&poketwo=Swoobat&pokeonefound=false&poketwofound=false

//NB: This Code isn't the most efficient as it could be as we wanted to load up the server to
// make it more likley to scale based on CPU ussage

// This section will change for Cloud Services
//const redisClient = redis.createClient(6379, 'asscloud2rovick.km2jzi.ng.0001.apse2.cache.amazonaws.com');
const redisClient = redis.createClient();
redisClient.on("error", (err) => {
  console.log("Error " + err);
});

require("dotenv").config();
const AWS = require("aws-sdk");
// Create unique bucket name
const bucketName = "n10419683-bucket-ass2";

app.get("/", (req, res) =>{

    res.status(200).json({ source: 'POKE API v2'});

});

app.use(responseTime());
app.use(apisavepokeone);
app.use(apisavepoketwo);

app.get("/api/search", (req, res) => {
  const queryone = req.query.pokeone.trim().toLowerCase();
  const querytwo = req.query.poketwo.trim().toLowerCase();

  //console.log(queryone);
  //console.log(querytwo);

   if (req.query.pokeonefound === "true" && req.query.poketwofound === "true") {
    const redisKey = `Poke:${queryone}`;

    const redisKeytwo = `Poke:${querytwo}`;

    return redisClient.get(redisKey, (err, pokeone) => {
      if (pokeone) {
        return redisClient.get(redisKeytwo, (err, poketwo) => {
          if (poketwo) {
            var pokeonename = "";
            var poketwoname = "";
            var pokeonearray = "";
            var poketwoarray = "";
            // Serve from Cache
            const resultpokeone = JSON.parse(pokeone);
            const resultpoketwo = JSON.parse(poketwo);
            if (resultpokeone.resultJSON2) {
              pokeonename = resultpokeone.resultJSON2.Pokemon;
              pokeonearray = resultpokeone.resultJSON2.responseJSON2;
            } else {
              pokeonename = resultpokeone.Pokemon;
              pokeonearray = resultpokeone.responseJSON2;
            }

            if (resultpoketwo.resultJSON2) {
              poketwoarray = resultpoketwo.resultJSON2.responseJSON2;
              poketwoname = resultpoketwo.resultJSON2.Pokemon;
            } else {
              poketwoname = resultpoketwo.Pokemon;
              poketwoarray = resultpoketwo.responseJSON2;
            }

            var healthpokeone = pokeonearray[0].base_stat;
            var healthpoketwo = poketwoarray[0].base_stat;
            var rounds = 0;
            var pokeonewins = 0;
            var poketwowins = 0;
            var winner = "";

            while (rounds < 100000) {
              var roundtrue = 1;
              healthpokeone = pokeonearray[0].base_stat;
              healthpoketwo = poketwoarray[0].base_stat;
              // console.log("ROUND" + rounds);

              while (roundtrue) {
                var randomnumber = Math.floor(Math.random() * 4 + 1);
                var load =  Math.log(rounds + 1);
                
                //pokeone attack of defend
                if (pokeonearray[randomnumber].stat.name === "attack" || pokeonearray[randomnumber].stat.name === "special-attack") {
                  healthpoketwo = healthpoketwo - pokeonearray[randomnumber].base_stat;
                  //console.log(pokeonearray[randomnumber].base_stat);
                 // console.log("BOOM");
                }
                if (pokeonearray[randomnumber].stat.name === "defense" || pokeonearray[randomnumber].stat.name === "special-defense") {
                  healthpokeone = healthpokeone + pokeonearray[randomnumber].base_stat;
                  // console.log(healthpoketwo);
                  //console.log("BLOCK");
                }

                //poketwo attack or defend
                if (poketwoarray[randomnumber].stat.name === "attack" || poketwoarray[randomnumber].stat.name === "special-attack") {
                  healthpokeone = healthpokeone - poketwoarray[randomnumber].base_stat;
                  //console.log(pokeonearray[randomnumber].base_stat);
                  //console.log("BOOM");
                }

                if (poketwoarray[randomnumber].stat.name === "defense" || poketwoarray[randomnumber].stat.name === "special-defense") {
                  healthpoketwo = healthpoketwo + pokeonearray[randomnumber].base_stat;
                  //console.log(healthpoketwo);
                  //console.log("BLOCK");
                }
                
                if (healthpokeone < 0) {
                  poketwowins = poketwowins + 1;
                  //console.log("Pokeone Wins ");
                  roundtrue = 0;
                } else {
                  pokeonewins = pokeonewins + 1;
                  // console.log("Poketwo Wins");
                  roundtrue = 0;
                }
              }

              rounds = rounds + 1;
            }

            if (pokeonewins > poketwowins) {
              winner = pokeonename;
            } else {
              winner = poketwoname;
            }

            return res.status(200).json({ winner: winner, pokeonename: pokeonename, pokeonewins: pokeonewins, pokeonearray, poketwoname: poketwoname, poketwowins: poketwowins, poketwoarray });
          }
        });
      } else {
        res.status(200).json({ ERROR: "NOTFOUND" });
      }
    });
  } else {
    res.status(200).json({ ERROR: "NOTFOUND" });
  }
});

//this saves or searches for the data and tells the server if it was found
function apisavepokeone(req, res, next) {
  const query = req.query.pokeone.trim().toLowerCase();
  //console.log(query);

  // Construct the poke URL and key pokeone
  const searchUrl = `https://pokeapi.co/api/v2/pokemon/${query}`;
  const redisKey = `Poke:${query}`;
  const s3Key = `Poke-${query}`;
  const params = { Bucket: bucketName, Key: s3Key };

  // Try the cache
  return redisClient.get(redisKey, (err, result) => {
    req.query.pokeonefound = "true";
    if (result) {
      // console.log(result)
      next();
      //return res.status(200).json(resultJSON);
    } else {
      return new AWS.S3({ apiVersion: "2006-03-01" }).getObject(params, (err, result) => {
        if (result) {
          // console.log(result);
          const resultJSON2 = JSON.parse(result.Body);
          redisClient.setex(redisKey, 3600, JSON.stringify({ resultJSON2 }));
          req.query.pokeonefound = "true";
          next();
          //return res.status(200).json(resultJSON);
        } else {
          // Serve from pokeapi API and store in cache
          return axios
            .get(searchUrl)
            .then((response) => {
              const responseJSON = response.data;
              const responseJSON2 = responseJSON.stats;
              redisClient.setex(
                redisKey,
                3600,
                JSON.stringify({
                  source: "Redis Cache",
                  Pokemon: query,
                  responseJSON2,
                })
              );
              const body = JSON.stringify({
                source: "S3 Bucket",
                Pokemon: query,
                responseJSON2,
              });
              const objectParams = {
                Bucket: bucketName,
                Key: s3Key,
                Body: body,
              };
              const uploadPromise = new AWS.S3({ apiVersion: "2006-03-01" }).putObject(objectParams).promise();
              uploadPromise.then(function (data) {
                console.log("Successfully uploaded data to " + bucketName + "/" + s3Key);
              });
              req.query.pokeonefound = "true";
              next();
              //return res.status(200).json({ source: 'POKE API v2',Pokemon: query ,responseJSON2, });
            })
            .catch((err) => {res.status(200).json({ ERROR: "NOTFOUND" });});
        }
      });
    }
  });
}

function apisavepoketwo(req, res, next) {
  const query = req.query.poketwo.trim().toLowerCase();
  //console.log(query);

  // Construct the poke URL and key pokeone
  const searchUrl = `https://pokeapi.co/api/v2/pokemon/${query}`;
  const redisKey = `Poke:${query}`;
  const s3Key = `Poke-${query}`;
  const params = { Bucket: bucketName, Key: s3Key };

  // Try the cache
  return redisClient.get(redisKey, (err, result) => {
    req.query.poketwofound = "true";
    if (result) {
      // Serve from Cache

      next();
      //return res.status(200).json(resultJSON);
    } else {
      return new AWS.S3({ apiVersion: "2006-03-01" }).getObject(params, (err, result) => {
        if (result) {
         // console.log(result);
          const resultJSON2 = JSON.parse(result.Body);
          redisClient.setex(redisKey, 3600, JSON.stringify({ resultJSON2 }));
          req.query.poketwofound = "true";
          next();
          //return res.status(200).json(resultJSON);
        } else {
          // Serve from pokeapi API and store in cache
          return axios
            .get(searchUrl)
            .then((response) => {
              const responseJSON = response.data;
              const responseJSON2 = responseJSON.stats;
              redisClient.setex(
                redisKey,
                3600,
                JSON.stringify({
                  source: "Redis Cache",
                  Pokemon: query,
                  responseJSON2,
                })
              );
              const body = JSON.stringify({
                source: "S3 Bucket",
                Pokemon: query,
                responseJSON2,
              });
              const objectParams = {
                Bucket: bucketName,
                Key: s3Key,
                Body: body,
              };
              const uploadPromise = new AWS.S3({ apiVersion: "2006-03-01" }).putObject(objectParams).promise();
              uploadPromise.then(function (data) {
                console.log("Successfully uploaded data to " + bucketName + "/" + s3Key);
              });
              req.query.poketwofound = "true";
              next();
              //return res.status(200).json({ source: 'POKE API v2',Pokemon: query ,responseJSON2, });
            })
            .catch((err) => {res.status(200).json({ ERROR: "NOTFOUND" });});
        }
      });
    }
  });
}

app.listen(3000, () => {
  console.log("Server listening on port: ", 3000);
});
